import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Battleship implements Runnable{
    int navalShipsLeft=0;
    int timeGameOn = 0;
    int run = 0;
    int globalSize;
    int globalShips;
    int shotsMissed=0;
    int shotsHited=0;
    long startTime = (System.currentTimeMillis()/1000);
    long endTime;
    boolean gameOn = true;
    private static final int DEFAULT_BOARD_SIZE =3;
    private static final int DEFAULT_BOARD_SHIP_AMOUNT =2;
    private static final int DEFAULT_GAME_TIME=120;
    private static final String DEFAULT_FILENAME ="progress.txt";

    public static void main(String[] args){
        if(args.length >= 2){
            try{
                int size =  Integer.parseInt(args[0]);
                int ships =  Integer.parseInt(args[1]);
                if(size !=0 && size >= 3){
					/*Battleship app = new Battleship(size, ships);
					Thread thread = new Thread(app);
					thread.start();*/
                    new Thread (new Battleship(size, ships)).start();
                }else{
                    System.out.println("Too small of a battlefield");
                }
            }catch(NumberFormatException e){
                System.out.println("I WILL FUCKING YEET U");
                System.exit(1);
            }
        }else{
            new Thread (new Battleship(DEFAULT_BOARD_SIZE, DEFAULT_BOARD_SHIP_AMOUNT)).start();
        }
    }

    //Contructor
    public Battleship(int size, int ships){
        System.out.println("STARTING");
        globalShips = ships;
        globalSize = size;
        int[][] numberArray = new int[size][size];
        int[][] playerViewArray = new int[(size+1)][(size+1)];
        for(int s =0; s<playerViewArray.length; s++){
            for(int k =0; k<playerViewArray[0].length; k++){
                if(s==0){
                    playerViewArray[s][k]=(k);
                }
            }
            playerViewArray[s][0]=(s);
        }
        sortMatrix(numberArray, ships, playerViewArray);
    }

    public void restart(){
        System.out.println("RESTARTING");
        int ships=globalShips;
        int size=globalSize;
        run = 0;
        startTime = (System.currentTimeMillis()/1000);
        int[][] numberArray = new int[size][size];
        int[][] playerViewArray = new int[(size+1)][(size+1)];
        gameOn=true;
        for(int s =0; s<playerViewArray.length; s++){
            for(int k =0; k<playerViewArray[0].length; k++){
                if(s==0){
                    playerViewArray[s][k]=(k);
                }
            }
            playerViewArray[s][0]=(s);
        }
        sortMatrix(numberArray, ships, playerViewArray);
    }

    private void sortMatrix(int[][] fun2Darray, int ships, int[][] playerViewArray){
        if(run ==0){
            int[][] battleReady = createNavalShips(ships, fun2Darray);
            ask(battleReady, playerViewArray);
        }else{
            ask(fun2Darray, playerViewArray);
        }
    }

    private void ask(int[][] battleReady, int[][] playerViewArray){
        Scanner scanner = new Scanner(System.in);
        String xStringCords="";
        String yStringCords="";
        if(gameOn){
            checkShipAmount(battleReady);
            print2Darray(playerViewArray, battleReady);
            if(gameOn){
                System.out.print("Enter x cord: ");
                xStringCords = scanner. next();
                if(xStringCords.equals("exit")){
                    saveBoard(battleReady);
                    System.out.println("I WILL FUCKING YEET U");
                    System.exit(1);
                }else if(xStringCords.equals("resume")){
                    battleReady=loadBoard();
                    playerViewArray=loadPlayerBoard(battleReady);
                }else if(xStringCords.equals("show")){
                    playerViewArray=showPlayerBoard(battleReady, playerViewArray);
                }else {
                    System.out.print("Enter y cord: ");
                    yStringCords = scanner.next();
                    if (yStringCords.equals("exit")) {
                        System.out.println("I WILL FUCKING YEET U");
                        System.exit(1);
                    }
                }
                try{
                    int xCords =  (Integer.parseInt(xStringCords));
                    int yCords =  (Integer.parseInt(yStringCords));
                    if(xCords != 0 && yCords!=0 && xCords >= 0 && yCords >= 0){
                        boolean hits = hitOrMiss((yCords-1), (xCords-1), battleReady);
                        if(hits){System.out.println("Hit or miss, I guess they never miss, huh?");}
                        hitTarget(hits, playerViewArray, battleReady, (yCords-1), (xCords-1));
                        run += 1;
                        sortMatrix(battleReady, 0, playerViewArray);
                    }else{
                        ask(battleReady, playerViewArray);
                    }
                }catch(NumberFormatException e){
                    ask(battleReady, playerViewArray);
                }
            }
        }else{
            System.out.print("Restart? (y/n)");
            String restart = scanner.next();
            if(restart.trim().toLowerCase().equals("y") || restart.trim().toLowerCase().equals("yes")){
                gameOn=true;
                restart();
            }else{
                System.exit(1);
            }
        }
    }

    private void hitTarget(boolean hited, int[][] playerViewArray, int[][] battleReady, int yCords, int xCords){
        if(hited){
            shotsHited+=1;
            battleReady[yCords][xCords] = 4;
            playerViewArray[(yCords + 1)][(xCords + 1)] = 4;
        }else{
            if(yCords<=(battleReady.length-1)){
                if(xCords<=(battleReady[0].length-1)){
                    if(battleReady[yCords][xCords]!=4){
                        shotsMissed+=1;
                        battleReady[yCords][xCords] = 9;
                        playerViewArray[(yCords + 1)][(xCords + 1)] = 9;
                    }
                }
            }
        }
        ask(battleReady, playerViewArray);
    }

    private boolean hitOrMiss(int yCords, int xCords, int[][] battleReady){
        if(yCords<=(battleReady.length-1)){
            if(xCords<=(battleReady[0].length-1)){
                int checkIfShip = battleReady[yCords][xCords];
                System.out.println(checkIfShip + " Cords: " +yCords+" | "+xCords);
                if(checkIfShip==1){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    private int[][] createNavalShips(int ships, int[][] battleReading){
        while(ships!=navalShipsLeft) {
            for (int s = 0; s < ships; s++) {
                int rand1 = randInt(0, (battleReading.length - 1));
                int rand2 = randInt(0, (battleReading.length - 1));
                if (battleReading[rand1][rand2] != 1) {
                    battleReading[rand1][rand2] = 1;
                    checkShipAmount(battleReading);
                }
            }
        }
        return battleReading;
    }

    private void print2Darray(int[][] printingArray, int[][] battleReady){
        System.out.println("Ships left: " + navalShipsLeft);
        endTime = (System.currentTimeMillis()/1000);
        System.out.println("Your time: " + (endTime-startTime+timeGameOn));
        if((endTime-startTime+timeGameOn)>=DEFAULT_GAME_TIME){
            gameOver();
            gameOn=false;
        }
        if(!gameOn){
            printingArray = showPlayerBoard(battleReady, printingArray);
        }
        for(int s =0; s<printingArray.length; s++){
            for(int k =0; k<printingArray[s].length; k++){
                if(k==0 && s==0){
                    System.out.print("  ");
                }else if(k==0 && s!=0){
                    System.out.print(" " + printingArray[s][k]);
                }else{
                    System.out.print(" | " + printingArray[s][k]);
                }
            }
            System.out.println();
        }
        if(navalShipsLeft==0){
            gameOn=false;
            endTime = (System.currentTimeMillis()/1000);
            gameOver();
        }

    }

    private int[][] showPlayerBoard(int[][] battleReady, int[][] playerBoard){
        for (int s = 0; s <battleReady.length; s++) {
            for (int k = 0; k <battleReady[s].length ; k++) {
                playerBoard[(s+1)][(k+1)] = battleReady[s][k];
            }
        }
        return playerBoard;
    }

    private void saveBoard(int[][] battleReady){
        try {
            FileWriter fileWriter = new FileWriter(DEFAULT_FILENAME);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            for (int s = 0; s < battleReady.length; s++) {
                for (int k = 0; k <battleReady[s].length ; k++) {
                    bufferedWriter.write((String.valueOf(battleReady[s][k])));
                }
                if(s < battleReady.length-1){
                    bufferedWriter.newLine();
                }
            }
            bufferedWriter.newLine();
            bufferedWriter.write(String.valueOf((endTime-startTime+timeGameOn)));
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int[][] loadBoard(){
        List<String> lines = new ArrayList<>();
        try {
            FileReader fileReader = null;
            fileReader = new FileReader(DEFAULT_FILENAME);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line = null;
            while((line = bufferedReader.readLine()) != null){
                lines.add(line);
            }
            bufferedReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Loading value");
        int[][] board=new int[((lines.get(0).length()))][(lines.size()-1)];
        for (int s = 0; s < lines.size(); s++) {
            String line = lines.get(s);
            for(int k = 0; k < line.length();k++){
                if((lines.size()-1)==s){
                    timeGameOn += line.charAt(k);
                }else{
                    char value = line.charAt(k);
                    try{
                        board[s][k] = Integer.parseInt(String.valueOf(value));
                    }catch(ArrayIndexOutOfBoundsException exception) {
                    }
                }
            }
        }
        return board;
    }

    private int[][] loadPlayerBoard(int[][] gameBoard){
        int[][] playerBoard = new int[(gameBoard.length +1)][(gameBoard[0].length + 1)];
        for(int s =0; s<playerBoard.length; s++){
            for(int k =0; k<playerBoard[0].length; k++){
                if(s==0){
                    playerBoard[s][k]=(k);
                }
            }
            playerBoard[s][0]=(s);
        }
        for (int s = 0; s < gameBoard.length; s++) {
            for(int k = 0; k < gameBoard.length;k++) {
                int gameBoardNrOnXY = gameBoard[s][k];
                switch (gameBoardNrOnXY){
                    case 4:
                        shotsHited +=1;
                        playerBoard[(s+1)][(k+1)] = gameBoard[s][k];
                        break;
                    case 9:
                        shotsMissed +=1;
                        playerBoard[(s+1)][(k+1)] = gameBoard[s][k];
                        break;
                    default:
                        playerBoard[(s+1)][(k+1)] = 0;
                        break;
                }
            }
        }
        return playerBoard;
    }

    private void checkShipAmount(int[][] battleReady){
        navalShipsLeft=0;
        for(int s =0; s<battleReady.length; s++){
            for(int k =0; k<battleReady[0].length; k++){
                if(battleReady[s][k]==1){
                    navalShipsLeft = navalShipsLeft + 1;
                }
            }
        }
    }

    public void gameOver(){
        System.out.println("Game Over");
        System.out.println("Your time: " + (endTime-startTime+timeGameOn));
        System.out.println("Shots missed: " + (shotsMissed));
        System.out.println("Shots hited: " + (shotsHited));
    }

    public static int randInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }

    public void run(){

    }
}
