package com.example.todoapp;

import android.text.format.DateFormat;

import java.util.Calendar;
import java.util.Date;

public class ToDoTask {
    public boolean completed;
    public long createdDate;
    public String description;
    public String key;

    public ToDoTask(){}

    public ToDoTask( String desc){
        this.completed = false;
        this.createdDate = Calendar.getInstance().getTimeInMillis();
        this.description = desc;
    }


}
