package com.example.todoapp;

import android.content.Context;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CustomArrayAdapter extends ArrayAdapter<ToDoTask> {
    private MainActivity mainActivity;

    public CustomArrayAdapter(MainActivity mainActivity, List<ToDoTask> toDoTasks){
        super(mainActivity.getApplicationContext(), 0, toDoTasks);
        this.mainActivity = mainActivity;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        final TaskViewHolder viewHolder;

        /*final RESTService restService = retrofit.create(RESTService.class);*/
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_task, parent, false);
            viewHolder = new TaskViewHolder();
            viewHolder.date = convertView.findViewById(R.id.text_date);
            viewHolder.description = convertView.findViewById(R.id.text_description);
            viewHolder.delete = convertView.findViewById(R.id.button_delete);
            viewHolder.checkBox = convertView.findViewById(R.id.checkbox_taskCompletion);
        }else {
            viewHolder = (TaskViewHolder) convertView.getTag();
        }
        final ToDoTask toDoTask = getItem(position);


        viewHolder.description.setText(toDoTask.description);
        viewHolder.date.setText(String.valueOf(DateFormat.format("dd-MM-yyyy (HH:mm:ss)", toDoTask.createdDate)));
        viewHolder.key = (toDoTask.key);
        viewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.delTask(getItem(position));
                remove(getItem(position));

                /*Call<ToDoTask> call = restService.deleteTask(getItem(position).id);
                call.enqueue(new Callback<ToDoTask>() {
                    @Override
                    public void onResponse(Call<ToDoTask> call, Response<ToDoTask> response) {

                    }

                    @Override
                    public void onFailure(Call<ToDoTask> call, Throwable t) {

                    }
                });*/
            }
        });
        viewHolder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toDoTask.completed = viewHolder.checkBox.isChecked();
                mainActivity.updateTask(toDoTask);

                /*Call<ToDoTask> call = restService.updateTask(toDoTask.id, toDoTask);
                call.enqueue(new Callback<ToDoTask>() {
                    @Override
                    public void onResponse(Call<ToDoTask> call, Response<ToDoTask> response) {
                    }

                    @Override
                    public void onFailure(Call<ToDoTask> call, Throwable t) {
                    }
                });*/
            }
        });
        viewHolder.checkBox.setChecked(toDoTask.completed);
        convertView.setTag(viewHolder);


        return convertView;
    }
    public class TaskViewHolder {
        public String key;
        public TextView date;
        public TextView description;
        public Button delete;
        public CheckBox checkBox;

    }
}
