package com.example.todoapp;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private List<ToDoTask> toDoTasks;
    private ListView listView;
    private ArrayAdapter<ToDoTask> listAdapter;
    private final String BASEURL = "http://7984e04d.ngrok.io";
    private final String emailTam = "tammiktanar@gmail.com";
    private final String passwordTam = "Tammik25";

    private Button buttonAdd;
    private EditText inputTask;
    private FirebaseAuth auth;
    private DatabaseReference database;
    private boolean firstRun = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        auth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance().getReference();

        /*Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASEURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final RESTService restService = retrofit.create(RESTService.class);
        Call<List<ToDoTask>> call = restService.getTasks();
        call.enqueue(new Callback<List<ToDoTask>>() {
            @Override
            public void onResponse(Call<List<ToDoTask>> call, Response<List<ToDoTask>> response) {
                List<ToDoTask> toDoTasks = response.body();
                if(toDoTasks != null && response.body().size() >0) {
                  for (int i = 0; i < toDoTasks.size(); i++) {
                        ToDoTask task = toDoTasks.get(i);
                        listAdapter.add(task);
                    }
                    for (ToDoTask toDoTask : toDoTasks) {
                        listAdapter.add(toDoTask);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<ToDoTask>> call, Throwable t) {
                Log.d("TODOApp", t.getMessage());
            }
        });*/
        buttonAdd = findViewById(R.id.button_add);
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String description = inputTask.getText().toString();
                if(!description.trim().isEmpty()) {
                    ToDoTask addToDoTask = new ToDoTask(description.trim());
                    inputTask.setText("");
                    listView.setSelection(listAdapter.getCount()-1);
                    addTask(addToDoTask);
                    /*Call<ToDoTask> call = restService.createTask(addToDoTask);
                    call.enqueue(new Callback<ToDoTask>() {
                        @Override
                        public void onResponse(Call<ToDoTask> call, Response<ToDoTask> response) {

                        }

                        @Override
                        public void onFailure(Call<ToDoTask> call, Throwable t) {

                        }
                    });*/
                }
            }
        });

        inputTask = findViewById(R.id.input_task);
        listView = findViewById(R.id.list_tasks);

        toDoTasks = new ArrayList<>();
        /*
        toDoTasks.add(new ToDoTask("Call fat prick"));
        toDoTasks.add(new ToDoTask("Finish homework"));
        toDoTasks.add(new ToDoTask("Send CV"));
        toDoTasks.add(new ToDoTask("Meeting at 15:00"));
        */

//        listAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, toDoTasks);
        listAdapter = new CustomArrayAdapter(this, toDoTasks);
        listView.setAdapter(listAdapter);
        startFirstRun();
    }


    private void signUp(String email, String password){
        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Toast.makeText(getApplicationContext(), "Account created!", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getApplicationContext(), "Fail!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void logIn(String email, String password){
        auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Toast.makeText(getApplicationContext(), "You are logged in!", Toast.LENGTH_SHORT).show();
                        loadAllTasks();
                    }
                });
    }

    private void loadAllTasks() {
        database.child("tasks").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listAdapter.clear();
                for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                    ToDoTask task = snapshot.getValue(ToDoTask.class);
                    listAdapter.add(task);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void addTask(ToDoTask task){
        task.key = database.child("tasks").push().getKey();
        database.child("tasks").child(task.key).setValue(task);
    }

    public void delTask(ToDoTask task){
        database.child("tasks").child(task.key).setValue(null);
    }

    public void updateTask(ToDoTask task){
        database.child("tasks").child(task.key).setValue(task);
    }

    public void startFirstRun(){
        if(firstRun){
            firstRun=false;
            loadAllTasks();
        }
    }


}
