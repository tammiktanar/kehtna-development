package com.example.calculatorapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {
    public static int REQUEST_CODE = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Intent intent = getIntent();
        String ans = intent.getStringExtra("ans");
        EditText ansTextField = (EditText)findViewById(R.id.TextField2);
        ansTextField.setText(ans);

    }


    public void onCalculatorButton(View view){
        Button button = (Button) view;
        String text = button.getText().toString();
        Intent cameraIntent = new Intent( "android.media.action.IMAGE_CAPTURE");
        switch (text){
            case "Take a pic":
            startActivityForResult(cameraIntent, REQUEST_CODE);
            break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ImageView imageView = (ImageView)findViewById(R.id.imageView);
        if(requestCode == REQUEST_CODE && resultCode == RESULT_OK){
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap)extras.get("data");
            imageView.setImageBitmap(imageBitmap);
        }
    }

    public void finish(View view){

    }
}
