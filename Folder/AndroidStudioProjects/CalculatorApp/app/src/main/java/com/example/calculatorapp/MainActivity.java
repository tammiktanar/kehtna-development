package com.example.calculatorapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
    private String stNumberEntered="";
    private String ndNumberEntered="";
    private boolean stNumberActive=true;
    private boolean ndNumberActive=false;
    private boolean stClickFunc=true;
    private boolean stClickDot=true;
    private String functionForCalc="";
    private String nrAns = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void changeNrSelected(String func){
        if(!stNumberEntered.isEmpty() && stClickFunc) {
            functionForCalc=func;
            stClickFunc = false;
            stNumberActive = !stNumberActive;
            ndNumberActive = !ndNumberActive;
        }
        Toast.makeText(getApplicationContext(), Boolean.toString(ndNumberActive), Toast.LENGTH_SHORT).show();
    }

    public void clrNr(){
        stNumberEntered="";
        ndNumberEntered="";
        stNumberActive=true;
        ndNumberActive=false;
        stClickFunc = true;
        stClickDot = true;
        functionForCalc="";
        nrAns = "";
    }

    public void clrLatestNrs(){
        if(stNumberActive){
            stNumberEntered="";
            stClickDot = true;
        }else if(ndNumberActive){
            ndNumberEntered="";
            stClickDot = true;
        }
    }

    public void clrLastNr(){
        if(stNumberActive){
            if(!(stNumberEntered.length() > 0)){

            }
        }else if(ndNumberActive){

        }
    }


    public void onCalculatorButton(View view){
        Button button = (Button) view;
        String text = button.getText().toString();
        EditText calcTextField = (EditText)findViewById(R.id.plain_text_input);
        Intent intent = new Intent(this, Main2Activity.class);
        switch (text) {
            case "=":
                try{
                    double stNumberEnteredInt=Double.parseDouble(stNumberEntered);
                    double ndNumberEnteredInt=Double.parseDouble(ndNumberEntered);
                    switch (functionForCalc){
                        case "+":
                            nrAns = String.valueOf(stNumberEnteredInt + ndNumberEnteredInt);
                            calcTextField.setText(nrAns);
                            intent.putExtra("ans", nrAns);
                            startActivity(intent);
                            clrNr();
                            break;
                        case "-":
                            nrAns = String.valueOf(stNumberEnteredInt * ndNumberEnteredInt);
                            calcTextField.setText(nrAns);
                            intent.putExtra("ans", nrAns);
                            startActivity(intent);
                            clrNr();
                            break;
                        case "*":
                            nrAns = String.valueOf(stNumberEnteredInt * ndNumberEnteredInt);
                            calcTextField.setText(nrAns);
                            intent.putExtra("ans", nrAns);
                            startActivity(intent);
                            clrNr();
                            break;
                        case "/":
                            nrAns = String.valueOf(stNumberEnteredInt / ndNumberEnteredInt);
                            calcTextField.setText(nrAns);
                            intent.putExtra("ans", nrAns);
                            startActivity(intent);
                            clrNr();
                            break;
                        default:
                            break;
                    }
                }catch (NumberFormatException e){
                }
                break;
            case "+":
                changeNrSelected("+");
                break;
            case "-":
                changeNrSelected("-");
                break;
            case "/":
                changeNrSelected("/");
                break;
            case "*":
                changeNrSelected("*");
                break;
            case "C":
                clrNr();
                break;
            case "CE":
                clrLatestNrs();
                break;
            case "Kustuta":
                clrLastNr();
                break;
            case "ndAct":
                startActivity(intent);
                break;
            case ".":
                Toast.makeText(getApplicationContext(), Boolean.toString(stClickDot), Toast.LENGTH_SHORT).show();
                if(stClickDot){
                    if(stNumberActive) {
                        stNumberEntered = stNumberEntered +  ".";
                        calcTextField.setText(stNumberEntered);
                    }else if(ndNumberActive){
                        ndNumberEntered = ndNumberEntered + ".";
                        calcTextField.setText(ndNumberEntered);
                    }
                    stClickDot = false;
                }
                break;
            default:
                if(stNumberActive) {
                    stNumberEntered += button.getText().toString();
                    calcTextField.setText(stNumberEntered);
                }else if(ndNumberActive){
                    ndNumberEntered += button.getText().toString();
                    calcTextField.setText(ndNumberEntered);
                }else{
                    Toast.makeText(getApplicationContext(), "Error 4", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
