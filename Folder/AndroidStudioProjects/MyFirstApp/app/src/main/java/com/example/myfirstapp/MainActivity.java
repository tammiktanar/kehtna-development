package com.example.myfirstapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private String stNumberEntered="";
    private String ndNumberEntered="";
    private boolean stNumberActive=true;
    private boolean ndNumberActive=false;
    private String functionForCalc="";
    private Button buttonTest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonTest = findViewById(R.id.button_press);
        buttonTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "WHY WAS I PRESSED", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void onCalculatorButton(View view){
        Button button = (Button) view;
        String text = button.getText().toString();
        switch (text) {
            case "=":
                Toast.makeText(getApplicationContext(), "WHY WAS I PRESSED", Toast.LENGTH_LONG).show();
                try{
                    int stNumberEnteredInt=Integer.parseInt("2");
                    int ndNumberEnteredInt=Integer.parseInt("3");
                    if(stNumberEnteredInt != 0 && stNumberEnteredInt > 0 && ndNumberEnteredInt != 0 && ndNumberEnteredInt > 0){
                        switch (functionForCalc){
                            case "+":
                                Toast.makeText(getApplicationContext(), "Im here", Toast.LENGTH_SHORT).show();
                                break;
                            default:
                                Toast.makeText(getApplicationContext(), "Error 1", Toast.LENGTH_SHORT).show();
                                break;
                        }
                    }else{
                        Toast.makeText(getApplicationContext(), "Error 2", Toast.LENGTH_SHORT).show();
                    }
                }catch (NumberFormatException e){
                    Toast.makeText(getApplicationContext(), "Error 3", Toast.LENGTH_SHORT).show();
                }
                break;
            case "+":
                stNumberActive=!stNumberActive;
                ndNumberActive=!ndNumberActive;
                functionForCalc="+";
                break;
            default:
                if(stNumberActive) {
                    stNumberEntered += button.getText().toString();
                    Toast.makeText(getApplicationContext(), stNumberEntered, Toast.LENGTH_SHORT).show();
                }else if(ndNumberActive){
                    ndNumberEntered += button.getText().toString();
                    Toast.makeText(getApplicationContext(), ndNumberEntered, Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getApplicationContext(), "Error 4", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
